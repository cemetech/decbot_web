from quotes.models import Quote

from decbot_web import DecbotListView
from django.views.generic import DetailView


class QuoteList(DecbotListView):
    context_object_name = "quotes"
    queryset = Quote.all_active()
    paginate_by = 20


class QuoteView(DetailView):
    context_object_name = "quote"
    queryset = Quote.all_active()
