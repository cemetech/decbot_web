from django.urls import path, re_path

from . import views

app_name = "quotes"
urlpatterns = [
    path("", views.QuoteList.as_view(), name="list"),
    re_path(r"^(?P<pk>\d+)$", views.QuoteView.as_view(), name="quote_view"),
]
