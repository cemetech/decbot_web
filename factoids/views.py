from decbot_web import DecbotListView

from django.http import Http404
from factoids.models import Factoids


class FactoidSummary(DecbotListView):
    model = Factoids
    context_object_name = "factoids"
    paginate_by = 50
