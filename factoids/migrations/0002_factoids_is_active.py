# Generated by Django 3.2 on 2024-08-03 08:07

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("factoids", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="factoids",
            name="is_active",
            field=models.BooleanField(db_column="active", default=True),
            preserve_default=False,
        ),
    ]
