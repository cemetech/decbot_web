from django.urls import path

from . import views

app_name = "factoids"
urlpatterns = [
    path("", views.FactoidSummary.as_view()),
]
