from django.db import models


class FactoidsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_active=True)


class Factoids(models.Model):
    command = models.CharField(
        db_column="call", max_length=50, null=False, primary_key=True
    )
    response = models.CharField(db_column="response", max_length=500, null=False)
    is_active = models.BooleanField(db_column="active")

    # Only objects which haven't been deleted.
    objects = FactoidsManager()
    # All objects, including those which have been soft-deleted.
    all_objects = models.Manager()

    class Meta:
        db_table = "factoids"
        ordering = ["command"]

    def __str__(self):
        return "{}: {}".format(self.command, self.response)
