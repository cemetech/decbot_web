FROM python:3.12-alpine AS build

ADD ./ /app
RUN pip install pdm && cd /app && pdm export | tee requirements.txt

FROM python:3.12-alpine

COPY --from=build /app/requirements.txt /app/
COPY ./dist/decbot_web-*.whl /app/
RUN cd /app && pip install -r requirements.txt && pip install decbot_web-*.whl

ENV DJANGO_SETTINGS_MODULE=decbot_web.settings_container
RUN DECBOT_WEB_SECRET_KEY=ignoreme manage.py collectstatic

ENTRYPOINT ["gunicorn", "decbot_web.wsgi:application"]
CMD ["-k", "gthread", "--threads", "4", "--capture-output"]
