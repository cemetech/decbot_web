import datetime as dt
from unittest import mock

from django.test import TestCase
from django.urls import reverse

from karma.models import Score, ScoreLog


class KarmaDetailViewTests(TestCase):
    def test_plots_correct_data(self):
        Score.objects.create(name="Merthykins", score=3)
        ScoreLog.objects.bulk_create(
            [ScoreLog(name="Merthykins", change=1) for _ in range(3)]
            + [ScoreLog(name="Merthykins", change=2)]
        )
        # Ensure timestamps all fall within the same span after
        # the view truncates them.
        ScoreLog.objects.filter(change=1).update(
            timestamp=dt.datetime(2024, 3, 14, 4, 20, 0, tzinfo=dt.UTC)
        )
        # Except this one
        ScoreLog.objects.filter(change=2).update(
            timestamp=dt.datetime(2024, 3, 15, 5, 13, 22, tzinfo=dt.UTC)
        )

        with (
            mock.patch("karma.views.go", autospec=True) as mock_plotly,
            mock.patch(
                "karma.views.output_graph_html",
                autospec=True,
                return_value="GRAPH GOES HERE",
            ) as mock_output_graph,
        ):
            response = self.client.get(
                reverse("scores:score-detail", kwargs=dict(pk="Merthykins"))
            )

        # Expected data is plotted
        mock_plotly.Scatter.assert_called_once_with(
            x=[
                dt.datetime(2024, 3, 14, tzinfo=dt.UTC),
                dt.datetime(2024, 3, 15, tzinfo=dt.UTC),
            ],
            y=[3, 5],
            marker={"color": "salmon"},
            mode="lines",
            line_shape="hv",
        )
        # The figure is built with expected layout
        mock_plotly.Figure.assert_called_once_with(
            data=mock_plotly.Scatter.return_value,
            layout={
                "xaxis": {
                    "rangeselector": {
                        "buttons": [
                            {
                                "count": 1,
                                "label": "1m",
                                "step": "month",
                                "stepmode": "backward",
                            },
                            {
                                "count": 6,
                                "label": "6m",
                                "step": "month",
                                "stepmode": "backward",
                            },
                            {
                                "count": 1,
                                "label": "1y",
                                "step": "year",
                                "stepmode": "backward",
                            },
                            {
                                "step": "all",
                            },
                        ]
                    }
                }
            },
        )
        mock_output_graph.assert_called_once_with(
            mock_plotly.Figure.return_value,
            title="Score for Merthykins over time",
            x_label=None,
        )

        self.assertEqual(response.context["graph"], mock_output_graph.return_value)
