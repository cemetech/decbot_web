import datetime as dt

import plotly.graph_objects as go
from decbot_web import DecbotListView
from django.core.paginator import Page
from django.db.models import Case, Sum, When, Window
from django.db.models.functions import TruncDay, TruncHour, TruncMinute, Rank
from django.templatetags.static import static
from django.utils.safestring import mark_safe
from django.views.generic import DetailView

from karma.models import Score, ScoreLog


def output_graph_html(fig, title, x_label, y_scale="linear"):
    fig.update_layout(
        title=title,
        title_x=0.5,
        title_automargin=True,
        xaxis=dict(title=x_label),
        yaxis=dict(title="Score", type=y_scale, automargin=True),
        margin=dict(t=0, b=0, l=10, r=10, pad=4),
    )
    return mark_safe(
        fig.to_html(
            include_plotlyjs=static("karma/plotly-basic-2.34.0.min.js"),
            config=dict(displaylogo=False),
        )
    )


class ScoreSummary(DecbotListView):
    model = Score
    context_object_name = "things"
    paginate_by = 50

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .annotate(
                rank=Window(
                    expression=Rank(),
                    order_by="-score",
                )
            )
            .order_by("-score", "name")
        )

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        page: Page = ctx["page_obj"]

        names = [x.name for x in page]
        scores = [x.score for x in page]
        fig = go.Figure(
            data=go.Bar(
                x=names,
                y=scores,
                customdata=[x.rank for x in page],
                hovertemplate="<b>%{x}</b>: rank %{customdata}<br>%{y} points<extra></extra>",
                marker=dict(color="salmon"),
            )
        )
        ctx["graph"] = output_graph_html(
            fig,
            title=f"Scores for rank {page.start_index()}-{page.end_index()}",
            x_label="Name",
            y_scale="log",
        )
        return ctx


class ScoreDetail(DetailView):
    model = Score

    def get_scorelog(self):
        return ScoreLog.objects.filter(name=self.object.name)

    def get_graph(self):
        # Aggregate score changes over time, using lower temporal precision
        # for events that are further in the past.
        now = dt.datetime.now(tz=dt.UTC)
        records = (
            self.get_scorelog()
            .annotate(
                span=Case(
                    When(
                        timestamp__gt=now - dt.timedelta(days=7),
                        then=TruncMinute("timestamp"),
                    ),
                    When(
                        timestamp__gt=now - dt.timedelta(days=90),
                        then=TruncHour("timestamp"),
                    ),
                    default=TruncDay("timestamp"),
                )
            )
            .values("span")
            .annotate(span_change=Sum("change"))
            .order_by("span")
            .values_list("span", "span_change")
        )
        # Transform the changes back into a rolling total of the score
        timestamps = []
        scores = []
        last_score = 0
        for row in records:
            timestamps.append(row[0])
            last_score += row[1]
            scores.append(last_score)

        fig = go.Figure(
            data=go.Scatter(
                x=timestamps,
                y=scores,
                marker=dict(color="salmon"),
                mode="lines",
                # Run horizontal from one point, then step vertically up
                # to the next.
                line_shape="hv",
            ),
            layout=dict(
                xaxis=dict(
                    rangeselector=dict(
                        buttons=[
                            dict(
                                count=1, label="1m", step="month", stepmode="backward"
                            ),
                            dict(
                                count=6, label="6m", step="month", stepmode="backward"
                            ),
                            dict(count=1, label="1y", step="year", stepmode="backward"),
                            dict(step="all"),
                        ]
                    )
                )
            ),
        )
        return output_graph_html(
            fig, title=f"Score for {self.object.name} over time", x_label=None
        )

    def get_historic_summary(self):
        now = dt.datetime.now(tz=dt.UTC)
        ranges: list[tuple[str, dt.timedelta]] = [
            ("day", dt.timedelta(days=1)),
            ("week", dt.timedelta(days=7)),
            ("30 days", dt.timedelta(days=30)),
            ("90 days", dt.timedelta(days=90)),
            ("year", dt.timedelta(days=365)),
        ]

        [oldest] = self.get_scorelog().values_list("timestamp", flat=True).order_by(
            "timestamp"
        )[:1] or [now]
        step = ranges[-1][1]
        point = now - step
        i = 2
        while point > oldest:
            ranges.append((f"{i} years", step * i))
            point -= step
            i += 1

        return [
            (
                label,
                self.get_scorelog()
                .filter(timestamp__gt=now - delta)
                .aggregate(Sum("change"))["change__sum"]
                or 0,
            )
            for label, delta in ranges
        ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["log"] = self.get_historic_summary()

        last_log = (
            self.get_scorelog()
            .values_list("timestamp", flat=True)
            .order_by("-timestamp")[:1]
        )
        if last_log:
            context["last_updated"] = last_log[0]

        context["graph"] = self.get_graph()

        return context
