from django.urls import path, re_path

from . import views

app_name = "scores"
urlpatterns = [
    path("", views.ScoreSummary.as_view(), name="score-summary"),
    re_path(r"^(?P<pk>.+)$", views.ScoreDetail.as_view(), name="score-detail"),
]
