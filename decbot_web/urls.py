from django.views.generic.base import RedirectView
from django.urls import include, path

urlpatterns = [
    path(
        "",
        RedirectView.as_view(pattern_name="scores:score-summary", permanent=True),
        name="index",
    ),
    path("quotes/", include("quotes.urls")),
    path("scores/", include("karma.urls")),
    path("factoids/", include("factoids.urls")),
]
