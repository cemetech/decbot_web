from django.core.paginator import Page
from django.views.generic import ListView


class DecbotListView(ListView):
    """A regular ListView, but with elided_page_range set on the paginator.

    Use with the pagination.html template.
    """

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        paginator = ctx["paginator"]
        page: Page = ctx["page_obj"]
        paginator.elided_page_range = paginator.get_elided_page_range(page.number)
        return ctx
